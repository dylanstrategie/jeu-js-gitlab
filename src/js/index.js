import { Character, character_list } from "./char-gestion.js";
import { json } from "../text.json";
import { myGameboard } from "./gameboard-gen.js";
import { select_chars_in_adjacency } from "./attack-system";

myGameboard();

let char_focus;// = perso selectionné (c'est lui qu'on veut faire bouger)
let turn_state = "player"; //Who is currently playing. Swaps when the char calls end turn

export function add_character(health, health_max, attack, x, y, team, ap, ap_max) {
  let test_char = new Character(health, health_max, attack, x, y, team, ap, ap_max);
  let tile_pos = document.querySelector(`.x${test_char.x}.y${test_char.y}`);
  let char_div = document.createElement("div");
  char_div.className = 'charimage';
  char_div.style.backgroundImage = (team === "player") ? "url(../src/img/sword_blue.png)" : "url(../src/img/sword_red.png)";
  tile_pos.appendChild(char_div);
  char_div.addEventListener('click', function () {
    if (team === "player") {
      char_focus = test_char;
    }
    else {
      console.log("Can't select AI piece");
    }
  });
  test_char.div = char_div;//j'assigne char_div à la propriété qu'on vient de créer dans le character
  character_list.push(test_char);
  return test_char;
}

export function run_ai_turn() {
  turn_state = "ai"
  console.log(character_list);
  for (let index = 0; index < character_list.length; index++) {
    if (character_list[index].team === "ai") {
      character_list[index].run_ai();
    }
    turn_state = "player";
    console.log("AI processing is now over. Your turn");
  }
}

export function clear_char_gamestate(char) {
  console.log("Clearing char");
  char_focus = null;
}

let char1 = add_character(100, 100, 20, 3, 2, "player", 5, 5);
let char2 = add_character(100, 100, 20, 6, 7, "player", 5, 5);
let char3 = add_character(100, 100, 10, 8, 1, "ai", 5, 5);
let char4 = add_character(100, 100, 10, 4, 6, "ai", 5, 5);
/*
char1.doAttack(char2);
console.log(char2);

char2.move('38');
console.log(char2);

char1.move('39');
console.log(char1);
*/

document.addEventListener("keydown", function (event) {
  if (char_focus) {//je rajoute un if qui vérifiera si char_focus avant de faire un move
    char_focus.move(event.keyCode);
    let tile_pos = document.querySelector(`.x${char_focus.x}.y${char_focus.y}`);
    if (tile_pos) {
      tile_pos.appendChild(char_focus.div);
    }
    else {
      char_focus.x = char_focus.previousPos.x;
      char_focus.y = char_focus.previousPos.y;
    }
  }
});

let attack_button = document.querySelector(".attack-hud-button")

attack_button.addEventListener('click', function () {


  if (char_focus) {

    let adjacency_options = select_chars_in_adjacency(char_focus);
    let target;

    if (adjacency_options.length === 1) {
      target = adjacency_options[0];
    }
    if (adjacency_options.length > 1) {
      target = adjacency_options[Math.floor(Math.random() * adjacency_options.length)];
    }
    if (target) {
      char_focus.doAttack(target);

      let info = document.querySelector("#info");//je selectionne l'élement html où s'afficheront les infos du panel
      let p = document.createElement('p'); // je créé le paragraphe qui va contenir le texte du panel infos
      info.appendChild(p); // on insère le paragraphe dans la div dont l'id est "#info" et qu'on a assigné à la variable info
      p.textContent = `${char_focus} melee attacked ${target.name} for ${char_focus.attack} HP (${target.health} HP remaining)`
    }
  }
})