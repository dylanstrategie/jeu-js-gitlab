import { character_list } from "./char-gestion";

export function select_chars_in_adjacency(char_comparaison) {
    let valid_targets = []
    for (let index = 0; index < character_list.length; index++) {
        if(character_list[index].x === char_comparaison.x && (character_list[index].y === char_comparaison.y + 1 || character_list[index].y === char_comparaison.y - 1) || (character_list[index].y === char_comparaison.y && (character_list[index].x === char_comparaison.x + 1 || character_list[index].x === char_comparaison.x -1))){
            valid_targets.push(character_list[index]);
        }
    }
    return valid_targets;
}

