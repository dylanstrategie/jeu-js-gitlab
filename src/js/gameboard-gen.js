export function myGameboard() {

    let gameboard_size = 10;
    let gameboard = document.querySelector(".gameboard");

    for (let index = 0; index < gameboard_size; index++) {
        let tile_row = document.createElement('div');
        let y_coord = index + 1;
        tile_row.className = 'row tile-row';
        tile_row.style.height = 1/gameboard_size * 100 + "%";
        gameboard.appendChild(tile_row);
            for (let index = 0; index < gameboard_size; index++) {
                let tile = document.createElement('div');
                tile.className = `tile x${index+1} y${y_coord}`;
                tile.style.width = 1/gameboard_size * 100 + "%";
                tile_row.appendChild(tile);
            }
    }
}
